<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
date_default_timezone_set('UTC');//set default timezone to UTC
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;
require_once twinwoods_DIR."/vendor/autoload.php";
function Twinwoods_admin_menu(){
    if ( ! current_user_can( 'manage_options' ) ) {
        return;
    }
    session_start();
    require_once twinwoods_DIR."/vendor/autoload.php";
    ?>
    <style>
        .loading{
            color:#FFFFFF00;
            background-image:url(<?=twinwoods_URL?>/ajax-loader.gif);
            background-repeat: no-repeat;
            background-position: center;
            background-size:contain;
        }

    </style>
    <script type="text/javascript">
        const timezoneOffset=new Date().getTimezoneOffset()*60*1000*-1;
    </script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    <div class="wrap">
      <h1><?php esc_html( get_admin_page_title() ); ?></h1>
      <form action="options.php" method="post">
        <?php
        settings_fields("twinwoods_admin");
        do_settings_sections( 'twinwoods_admin' );
        submit_button('Save Settings');
        twinwoods_put_access_data_from_session(get_current_user_id());
        ?>
      </form>
      <h2>Outlook calendars options</h2>
      Here you can pick Outlook calendar where new booking events would be stored.<br><br>
      <?php 
      $res=true;
      $calendars=unserialize(get_option("twinwoods_outlook"));
      if (!$calendars){
        $calendars=Array();
      }
      //echo isset($calendars[get_current_user_id()])." isset";
      if(isset($calendars[get_current_user_id()]) and $calendars[get_current_user_id()]['expires']<=time()){
        $res=update_with_refresh_token();
        if (!($res === true)){
            echo "<br>".$res."<br>";
        }
        $calendars=unserialize(get_option("twinwoods_outlook"));
      }
      $access=twinwoods_is_outlook_has_access();
      if($access and $res === true){
        ?>
        <p style="color:green;font-weight: 800;">Application has access to your calendars</p>
        <?php
      } else{
        ?>
        <p style="color:red;font-weight: 800;">Application has no access to your calendars! Please permit access<br>
        to your calendars and then pick and save one in the dropdown.</p>
        <?php
      }
      try {
      } catch (Exception $e) {
          print_r($e);
      }
      

      if (!isset($calendars[get_current_user_id()])){
        echo "You haven't added any calendars yet";
      }
      /*elseif(!isset($calendars[get_current_user_id()]['access_token'])){
        echo "You picked '".$calendars[get_current_user_id()]['name']."' calendar";
      }*/
      echo "<br><br>";
      if ($access){
        try {

            $graph = new Graph();
            $graph->setAccessToken($calendars[get_current_user_id()]['access_token']);


            $calendarsOutlook = $graph->createRequest("GET", "/me/calendars")
                          ->setReturnType(Model\Calendar::class)
                          ->execute();
            //print_r($calendars);
            $time=date('Y-m-d\Th:i:s', time());
            $time_end=date('Y-m-d\Th:i:s', time()+36000);
            $graph = new Graph();
            $graph->setAccessToken($_SESSION['access_token']);
                       
            ?>
            <select id="calendar-selector" required>
            <option <?php if (!isset($calendars[get_current_user_id()])){ echo "selected"; } ?> ></option>
            <?php
            foreach ($calendarsOutlook as $value) {
                ?>
                    <option data-id="<?= $value->getId() ?>" <?php if(isset($calendars[get_current_user_id()]['id']) and $calendars[get_current_user_id()]['id']==$value->getId()){echo "selected";} ?> ><?= $value->getName(); ?></option>
                <?php
            }
            ?>
            </select>
            <br>
            <?php
        } catch (Exception $e) {
            echo "Can't get the calendars list from Outlook. Error:<br>";
            print_r($e);
        }
      }
      if ($access and $res === true){
        ?>
          <br><button class="button-primary" id="outlook-button">Save outlook settings</button><br>
          In case something is wrong - update permissions<br>
          <a class="button-primary" onclick="location.href='<?= twinwoods_URL ?>outlook.php';">Update permissions...</a>
        <?php
      }
      else{

        unset($_SESSION['access_token']);
        unset($_SESSION['refresh_token']);
        unset($_SESSION['expires']);
        unset($_SESSION['updated']);
        update_option("twinwoods_outlook", "");
        ?>

            <a class="button-primary" onclick="location.href='<?= twinwoods_URL ?>outlook.php';">Click to permit access to your calendars...</a>
        <?php
      }
      ?>
    </div>
    <script type="text/javascript">
        document.getElementById("outlook-button").addEventListener("click", function(){
            let cal=document.getElementById("calendar-selector");
            if (!cal.checkValidity()){
                cal.reportValidity();
                return;
            }
            let button=this;
            let selected=cal.options[cal.selectedIndex];
            let params={
                    url: "/wp-admin/admin-ajax.php",
                    method:"POST",
                    data:{
                        action:"save_the_calendar",
                        name:selected.innerText,
                        id:selected.dataset.id
                    }
                };
            button.classList.add("loading");
            $.ajax(params)
            .done(function(res){
                let result=JSON.parse(res);
                if (!result['result']) alert(result['message']);
                button.innerHTML="Saved!"
            })
            .fail(function(){
                alert("Can't save the calendar, something is wrong!");
            })
            .always(function(){
                button.classList.remove("loading");
            });
        });
    </script>
 <?php
}

function twinwoods_menu_on_hook(){
    add_menu_page(
    "Twinwoods booking",
    "Twinwoods",
    'manage_options',
    "Twinwoods_admin",
    'Twinwoods_admin_menu'
);
}

add_action('admin_menu', 'twinwoods_menu_on_hook');

function twinwoods_settings_init(){
    twinwoods_settings_update();
    add_settings_section(//our sectrion
    'twinwoods_settings',
    /*translators: general settings page*/
    'General',
    function(){},
    'twinwoods_admin'
    );
    add_settings_field(
    "twinwoods_category", 
    "Category with bookable products", 
    function(){
        $value=get_option("twinwoods_category");
        $categories = get_categories( [
            'taxonomy'     => 'product_cat',
            'orderby'      => 'name',
            'order'        => 'ASC',
            'hide_empty'   => 0,
        ] );
        ?>
        <select id='twinwoods_category' name='twinwoods_category'>

            <?php
               foreach ($categories as $cat) {
                   ?>
                   <option <?php
                        if ($value==$cat->name){
                            echo "selected";
                        }
                    ?>
                    >
                    <?= $cat->name ?></option>
                   <?php
               }
            ?>
        </select>
        <?php
    },
    "twinwoods_admin",
    'twinwoods_settings' 
    );
}

function twinwoods_settings_update(){ //registering settings here
    register_setting(
            "twinwoods_admin",
            "twinwoods_category"
    );
    register_setting(
            "twinwoods_admin",
            "twinwoods_outlook"
    );
    
}
add_action('admin_init', 'twinwoods_settings_init');//set admin menu

add_action( 'wp_ajax_add_slots', 'twinwoods_add_slots' );
function twinwoods_add_slots(){
    if ( ! current_user_can( 'manage_options' ) ) {
        return;
    }
    $respond=array('result'=>false, 'message'=>'All fine');
    date_default_timezone_set('UTC');
    global $wpdb;
    $date=date("Y-m-d", $_POST['timestamp']/1000);
    $slots=$wpdb->get_results( "SELECT * FROM ".$wpdb->prefix."twinwoods_bookings WHERE DATE(time) = '$date'", ARRAY_A);
    for ($i=0; $i < $_POST['repeat']; $i++) { 
        $timestamp=$_POST['timestamp']+$_POST['duration']*3600000*$i;
        foreach ($slots as $slot) {
            error_log($slot['timestamp']);
            error_log($_POST['timestamp']);
            if($slot['timestamp']<=$timestamp and $slot['timestamp']+$slot['duration']*3600000>$timestamp){
                $respond['message']="Failed to create new booking slot - it's time frame intersects with existing slot with id ".$slot['id'];
                echo json_encode($respond);
                wp_die();
            }
        }
        $time=date("Y-m-d H:i:s", $timestamp/1000);
        $wpdb->insert($wpdb->prefix."twinwoods_bookings", array('time'=>$time, 'product_id'=>$_POST['product_id'],'timestamp'=>$timestamp, 'duration'=>$_POST['duration'] ,'slots'=>$_POST['slots']));
    }
    $respond['result']=true;
    echo json_encode($respond);
    wp_die();
}

add_action( 'wp_ajax_remove_slot', 'twinwoods_delete_slots' );
function twinwoods_delete_slots(){
    if ( ! current_user_can( 'manage_options' ) ) {
        return;
    }
    global $wpdb;
    $wpdb->delete($wpdb->prefix."twinwoods_bookings", array('id'=>$_POST['slot_id']));
}

function render_admin_slots($timestamp){
    date_default_timezone_set('UTC');
    global $wpdb;
    $date=date("Y-m-d", $timestamp);
    $slots=$wpdb->get_results( "SELECT * FROM ".$wpdb->prefix."twinwoods_bookings WHERE DATE(time) = '$date'", ARRAY_A);
    foreach ($slots as $slot) {
        ?>
        <li id="id<?= $slot['id'] ?>" data-id="<?= $slot['id'] ?>" 
            data-utc-time="<?= $slot['time'] ?>" 
            data-stamp="<?= $slot['timestamp'] ?>" 
            data-duration="<?= $slot['duration'] ?>">
            <button class="delete-slot">X</button>
            <div class="time-period">
                <span class="time-begin">
                </span> - 
                <span class="time-end">
                </span>
            </div>

            
        </li>
        <script>
            document.getElementById("id<?= $slot['id'] ?>").querySelector(".time-begin").innerHTML=new Date(<?= $slot['timestamp'] ?>+timezoneOffset).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
            document.getElementById("id<?= $slot['id'] ?>").querySelector(".time-end").innerHTML=new Date(<?= $slot['timestamp'] ?>+timezoneOffset+<?= $slot['duration'] ?>*3600000).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
        </script>
        <?php
    }
}


function twinwoods_is_outlook_has_access($user_id=0){
    $calendars=unserialize(get_option('twinwoods_outlook'));
    if ($user_id==0){
        $user_id=get_current_user_id();
    }
    if (!$calendars and !isset($_SESSION['access_token'])){
        return false;
}
    if (!$calendars){
        $calendars=array();
        $option=array('expires'=>$_SESSION['expires'], 'access_token'=>$_SESSION['access_token'],'refresh_token'=>$_SESSION['refresh_token']);
    } else {
        $option=$calendars[$user_id];
    }
    if (!isset($option['access_token'])){
        return false;
    }
    return true;
    try {

            $graph = new Graph();
            $graph->setAccessToken($calendars[get_current_user_id()]['access_token']);


           $user = $graph->createRequest("GET", "/me/contacts")
                  ->setReturnType(Model\Event::class)
                  ->execute();
         } catch (Exception $e) {
            print_r($e);
            return false;
        }

    return true;
}

function update_with_refresh_token($user_id=0){
    $calendars=unserialize(get_option('twinwoods_outlook'));
    if ($user_id==0){
        $user_id=get_current_user_id();
    }
    if (!$calendars){
        $calendars=array();
        $option=array();
    } else {
        $option=$calendars[$user_id];
    }
    if (!$option['refresh_token']){
        return "No refresh token found!";
    }
    $refresh_token=$option['refresh_token'];
    try {
        require_once twinwoods_DIR."/vendor/autoload.php";
        $guzzle = new \GuzzleHttp\Client();
        $url = 'https://login.microsoftonline.com/common/oauth2/v2.0/token';
        $token = json_decode($guzzle->post($url, [
            'form_params' => [
                'client_id' => "4aaa9efc-a8c0-4337-b686-1c2e9942b0fe",
                'client_secret' => "T7rkaps5M_-UzEAPyO3Llog_4t9JSwD6P-",
                'scope' => "profile openid email offline_access User.Read Mail.Send Calendars.ReadWrite Contacts.Read",
                'grant_type' => 'refresh_token',
                'refresh_token' => $refresh_token
            ],
        ])->getBody()->getContents());
        $_SESSION['access_token']=$token->access_token;
        $_SESSION['refresh_token']=$token->refresh_token;
        $_SESSION['expires']=time()+$token->expires_in;
        twinwoods_put_access_data_from_session($user_id);
        
    } catch (Exception $e) {
        print_r($e);
        return "Something is wrong with the refreshing Token!";
    }
    
    return true;
}

function twinwoods_put_access_data_from_session($user_id){
    $calendars=unserialize(get_option('twinwoods_outlook'));
    if (!isset($user_id)){
        $user_id=get_current_user_id();
    }
    if (!$calendars){
        $calendars=array();
    }
    if (isset($calendars[$user_id]['updated']) and $calendars[$user_id]['updated']<$_SESSION['updated']){
        return;
    }
    $calendars[$user_id]['access_token']=$_SESSION['access_token'];
    $calendars[$user_id]['refresh_token']=$_SESSION['refresh_token'];
    $calendars[$user_id]['expires']=$_SESSION['expires'];
    $calendars[$user_id]['updated']=$_SESSION['updated'];
    update_option('twinwoods_outlook', serialize($calendars));
}

function twinwoods_save_the_calendar(){
    $result=array('result'=>true);
    $calendars=unserialize(get_option("twinwoods_outlook"));
    if (count($calendars)<1){
        $calendars=array();
    }
    $calendars[get_current_user_id()]['name']=$_POST['name'];
    $calendars[get_current_user_id()]['id']=$_POST['id'];
    update_option('twinwoods_outlook', serialize($calendars));
    echo json_encode($result);
    wp_die();
}

add_action( 'wp_ajax_save_the_calendar', 'twinwoods_save_the_calendar' );

function twinwoods_add_event_to_outlook($id){
    $order=wc_get_order($id);
    $admins=unserialize(get_option("twinwoods_outlook"));
    if (!$admins){
        error_log("NO ADMINS??");
        return;
    }
    $email=$order->get_billing_email();
    $name=$order->get_billing_first_name()." ".$order->get_billing_last_name();

    $items=$order->get_items();
    foreach ($items as $item) {
        foreach ($admins as $key => $admin) {
            //try {
                if($admins[$key]['expires']<=time()){
                $res=update_with_refresh_token();
                if (!($res === true)){
                    echo "<br>Something wrong with updating token: <br>".$res."<br>";
                    throw new Exception("Problem with token updating");
                }
                $admins=unserialize(get_option("twinwoods_outlook"));
                }
                $graph = new Graph();
                $graph->setAccessToken($admins[$key]['access_token']);
                $time=date('Y-m-d\Th:i:s', $item->get_meta( 'Timestamp', true ));
                $end=date('Y-m-d\Th:i:s', $item->get_meta( 'Timestamp', true )+$item->get_meta( 'Duration', true ));
                $timezone=$item->get_meta( 'Customer timezone name', true );
                $screens=$item->get_meta( 'Screens', true );
                $product_name=$item->get_name();
                $order_page=get_site_url()."/wp-admin/post.php?post=$id&action=edit";
                $result=<<<EOD
                {
                  "subject": "Booking event",
                  "body": {
                    "contentType": "HTML",
                    "content": "<h3>You got a new booking!</h3>Booked product is <strong>$product_name</strong><br>Number of screens is <strong>$screens</strong><br><br>See details on the <a href='$order_page'>Order page</a>"
                  },
                  "start": {
                      "dateTime": "$time",
                      "timeZone": "$timezone"
                  },
                  "end": {
                      "dateTime": "$end",
                      "timeZone": "$timezone"
                  },
                  "location":{
                      "displayName":"Twinwoods"
                  },
                  "attendees": [
                    {
                      "emailAddress": {
                        "address":"$email",
                        "name": "$name"
                      },
                      "type": "required"
                    }
                  ]
                }
EOD;
                    $calendarsOutlook = $graph->createRequest("POST", "/me/calendars/".$admins[$key]['id']."/events")
                  ->setReturnType(Model\Event::class)
                  ->attachBody(json_decode($result))
                  ->execute();
            //} catch (Exception $e) {
                //when can't place event
                //error_log("Can't place event! ");
            //}

        }
    }
}
add_action( 'woocommerce_checkout_update_order_meta', 'twinwoods_add_event_to_outlook');

function twinwoods_place_event($order_id=0){
    //test purposts only, delete later
    $order=wc_get_order(60);
    $item;
    foreach ($order->get_items() as $key => $value) {
        $item=$value;
    }
    $email=$order->get_billing_email();
    $name=$order->get_billing_first_name()." ".$order->get_billing_last_name();

    $time=date('Y-m-d\Th:i:s', $item->get_meta( 'Timestamp', true ));
    $end=date('Y-m-d\Th:i:s', $item->get_meta( 'Timestamp', true )+$item->get_meta( 'Duration', true ));
    $timezone=$item->get_meta( 'Customer timezone name', true );
    $screens=$item->get_meta( 'Screens', true );
    $product_name=$item->get_name();
    $order_page=get_site_url()."/wp-admin/post.php?post=$order_id&action=edit";
    $result=<<<EOD
                {
                  "subject": "Test event",
                  "body": {
                    "contentType": "HTML",
                    "content": "<h3>
                    You got a new booking!
                    </h3>
                    Booked product is <strong>$product_name</strong><br>
                    Number of screens is <strong>$screens</strong><br><br>
                    See details on the <a href='$order_page'>Order page</a>
                    "
                  },
                  "start": {
                      "dateTime": "$time",
                      "timeZone": "$timezone"
                  },
                  "end": {
                      "dateTime": "$end",
                      "timeZone": "$timezone"
                  },
                  "location":{
                      "displayName":"Twinwoods"
                  },
                  "attendees": [
                    {
                      "emailAddress": {
                        "address":"$email",
                        "name": "$name"
                      },
                      "type": "required"
                    }
                  ]
                }
EOD;

return $result;
}