<?php

require_once "vendor/autoload.php";


/*$guzzle = new \GuzzleHttp\Client();
$url = 'https://login.microsoftonline.com/common/oauth2/token?api-version=1.0';
$token = json_decode($guzzle->post($url, [
    'form_params' => [
        'client_id' => "4aaa9efc-a8c0-4337-b686-1c2e9942b0fe",
        'client_secret' => "T7rkaps5M_-UzEAPyO3Llog_4t9JSwD6P-",
        'resource' => 'https://graph.microsoft.com/',
        'grant_type' => 'client_credentials',
    ],
])->getBody()->getContents());*/
session_start();
$client_id="4aaa9efc-a8c0-4337-b686-1c2e9942b0fe";
if (!isset($_REQUEST['code'])){
header("Location: https://login.microsoftonline.com/common/oauth2/v2.0/authorize?state=80k3RFwZ5XuePHtrDSQRkl5FA0KMdUqo&scope=profile+openid+email+offline_access+User.Read+Mail.Send+Calendars.ReadWrite&response_type=code&approval_prompt=auto&client_id=$client_id&redirect_uri=https://twinwoods.uk/wp-content/plugins/twinwoods-booking/outlook.php");
die();
}

$guzzle = new \GuzzleHttp\Client();
$url = 'https://login.microsoftonline.com/common/oauth2/v2.0/token';
$token = json_decode($guzzle->post($url, [
    'form_params' => [
        'client_id' => "4aaa9efc-a8c0-4337-b686-1c2e9942b0fe",
        'client_secret' => "T7rkaps5M_-UzEAPyO3Llog_4t9JSwD6P-",
        'grant_type' => 'authorization_code',
        'code' => $_REQUEST['code'],
        //'scope' => 'profile openid email offline_access User.Read Mail.Send Calendars.ReadWrite',
        'redirect_uri' => 'https://twinwoods.uk/wp-content/plugins/twinwoods-booking/outlook.php'
    ],
])->getBody()->getContents());
$accessToken=$token->access_token;
$_SESSION['access_token']=$accessToken;
$_SESSION['refresh_token']=$token->refresh_token;
$_SESSION['expires']=time()+$token->expires_in;
$_SESSION['updated']=time();

//echo($_SESSION['access_token']."<br>".$_SESSION['refresh_token']."<br>".$_SESSION['expires']."<br>".time());
header("Location: https://twinwoods.uk/wp-admin/admin.php?page=Twinwoods_admin");

die();

use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;

$graph = new Graph();
$graph->setAccessToken($_SESSION['access_token']);


$calendars = $graph->createRequest("GET", "/me/calendars")
              ->setReturnType(Model\CalendarGroup::class)
              ->execute();
foreach ($calendars as $value) {
	echo "- ".$value->getName()."<br>";
}