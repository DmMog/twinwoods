const timezoneOffset=new Date().getTimezoneOffset()*60*1000*-1*0;
$("#datepicker").datepicker({
	onSelect: function(date){
		$('#datepicker_value').val(date);
		renderAllSlots();
	},
	minDate: 0
});
$("#datepicker").datepicker("setDate", Date.now());

function renderAllSlots(){
	[].forEach.call(document.querySelectorAll('.booking-item'), function(item){
		renderSlots($("#datepicker").val(), item.dataset.id, item.querySelector('.bookind-slots'));
	});
}
renderAllSlots();
function renderSlots(date, product_id, container){
    container.classList.add("loading");
    $(container).closest('.booking-item').find('.add-to-cart').attr('disabled', true);
    let timestamp=new Date(date).getTime();
    let params={
    url: "/wp-admin/admin-ajax.php",
    method:"POST",
    data:{
        action:"get_product_slots",
        timestamp:timestamp,
        product_id:product_id,
        offset:new Date().getTimezoneOffset()*60*-1
    }
    };
    $.ajax(params)
    .done(function(res){
        container.innerHTML=res;
        [].forEach.call(container.querySelectorAll('script'), function(item){
            eval(item.innerHTML);
        });
        try{
	        container.querySelector('[type="radio"]').checked="true";
	        $(container).closest('.booking-item').find('.add-to-cart').attr('disabled', false);
    	}catch{
    		$(container).closest('.booking-item').find('.add-to-cart').attr('disabled', true);
    	}
    })
    .fail(function(){
        alert("Something is wrong with loading time slots!");
    })
    .always(function(){
    	container.classList.remove("loading");
    });
};

$('.add-to-cart').on('click', function(){
	let product_id=$(this).closest('.booking-item').data('id');
	let monitorNumber=$(this).closest('.booking-item').find('[name="monitor-quantity"]').val();
	let slotId=$(this).closest('.booking-item').find('[type="radio"]:checked').val();
	this.classList.add("loading");
	let params={
    url: "/wp-admin/admin-ajax.php",
    method:"POST",
    data:{
        action:"twinwoods_add_to_cart",
        quantity:1,
        product_id:product_id,
        slot_id:slotId,
        monitor_number:monitorNumber,
        timezone_offset:new Date().getTimezoneOffset(),
        timezone_name:Intl.DateTimeFormat().resolvedOptions().timeZone
    	}
    };
    $.ajax(params)
    .done(function(res){
    	let result=JSON.parse(res);
    	console.log(res);
    	if (!result['result']){
    		alert(result['message']);
    		return;
    	}
    	let item=$(this).closest('.booking-item')[0];
    	renderSlots($("#datepicker").val(), item.dataset.id, item.querySelector('.bookind-slots'));
    	twin_updateCheckout();
    	twin_updateCart();
    	this.innerHTML="Added!";
    }.bind(this))
    .fail(function(){
        alert("Something is wrong with adding to cart!");
    })
    .always(function(){
    	this.classList.remove("loading");
    }.bind(this));
});

$('[name="monitor-quantity"]').on("input", function(){
	let priceTag=$(this).closest('.booking-item').find('.price');
	let noMonitorPrice=$(this).closest('.booking-item').find('.startPrice').text()*1;
	let addPrice= this.value<=4 ? this.value*20+noMonitorPrice : 20*4+(this.value-4)*15+noMonitorPrice;
	priceTag.html(addPrice);
});

function twin_updateCheckout(){
	let checkout = $('.checkout-wrap');
	checkout.addClass("loading");
	$.ajax({
		url: "/wp-admin/admin-ajax.php",
	    method:"POST",
	    data:{
	        action:"twinwoods_reload_checkout",
    	}
	})
    .done(function(res){
    	checkout.html(res);
    })
    .fail(function(){
        alert("Something is wrong with reloading checkout!");
    })
    .always(function(){
    	checkout.removeClass("loading");
    }.bind(this));
}

function twin_updateCart(){
	let checkout = $('.twin-cart-wrap');
	checkout.addClass("loading");
	$.ajax({
		url: "/wp-admin/admin-ajax.php",
	    method:"POST",
	    data:{
	        action:"twin_update_cart",
    	}
	})
    .done(function(res){
    	checkout.html(res);
    })
    .fail(function(){
        alert("Something is wrong with reloading cart!");
    })
    .always(function(){
    	checkout.removeClass("loading");
    }.bind(this));
}

[].forEach.call(document.querySelectorAll('.monitor-input-wrap'), item=>{
	item.addEventListener("click", function(e){
		if(e.target.classList.contains('monitor-button')){
			let input=$(this).find('input');
			let qt=input.val()*1;
			if (e.target.classList.contains('minus')){
				if (qt-1>=input.attr('min')*1){
					input.val(qt-1);
					qt--;
				}
			}
			if (e.target.classList.contains('plus')){
				if (qt+1<=input.attr('max')*1){
					input.val(qt+1);
					qt++;
				}
			}
			let priceTag=$(this).closest('.booking-item').find('.price');
			let noMonitorPrice=$(this).closest('.booking-item').find('.startPrice').text()*1;
			let addPrice= qt<=4 ? qt*20*currency_rate+noMonitorPrice : 20*4*currency_rate+(qt-4)*15*currency_rate+noMonitorPrice;
			addPrice=Math.floor(addPrice*100)/100;
			priceTag.html(addPrice.toFixed(2));
		}
	});
});

(function($){ 
	$( document.body ).on( 'updated_cart_totals removed_from_cart updated_wc_div', function(){
	    renderAllSlots();
	    twin_updateCheckout();
	});
})(jQuery)


$(".monitor-input-wrap").on("click", function(e){
	if(e.target.classList.contains('monitor-button')){
		console.log(this);
	}
	if(e.target.classList.contains('monitor-button')){
		
		
	}
});