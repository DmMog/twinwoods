<?php
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;
require_once "vendor/autoload.php";
session_start();

$time=date('Y-m-d\Th:i:s', time());
$time_end=date('Y-m-d\Th:i:s', time()+36000);
try {

            $graph = new Graph();
            $graph->setAccessToken($_SESSION['access_token']);

            $object = <<<EOD
            	{
				  "subject": "Test event",
				  "body": {
				    "contentType": "HTML",
				    "content": "Here are our test event!"
				  },
				  "start": {
				      "dateTime": "$time",
				      "timeZone": "Europe/Kiev"
				  },
				  "end": {
				      "dateTime": "$time_end",
				      "timeZone": "Europe/Kiev"
				  },
				  "location":{
				      "displayName":"Twinwoods"
				  },
				  "attendees": [
				    {
				      "emailAddress": {
				        "address":"test@test.com",
				        "name": "John Doe"
				      },
				      "type": "required"
				    }
				  ]
				}
EOD;

            $calendarsOutlook = $graph->createRequest("POST", "/me/calendars/AQMkADAwATNiZmYAZC1kNGM1LTBjZjEtMDACLTAwCgBGAAAD2QejTCqxG0q_6nGnSsCM0QcAN5Nkacu4jUqaGgsArXFPUgAAAgEGAAAAN5Nkacu4jUqaGgsArXFPUgAAAEYGF7kAAAA=/events")
                          ->setReturnType(Model\Event::class)
                          ->attachBody(json_decode($object))
                          ->execute();
            //print_r($calendars);
            ?>
			
            <?php
        } catch (Exception $e) {
            echo "Can't create event. Error:<br>";
            print_r($e);
        }

?>