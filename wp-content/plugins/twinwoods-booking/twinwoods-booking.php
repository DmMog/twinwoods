<?php
/*
Plugin Name: Twinwoods booking
Description: Custom booking system
Version: 1.0
Author: Dmitrij Mogil
Text Domain: twinwoods
*/

/*  Copyright 2019  Dmitrij Mogil  

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

define ("twinwoods_DIR", rtrim( plugin_dir_path( __FILE__ ), '/' ));
define ("twinwoods_URL", plugin_dir_url( __FILE__ ));

function twinwoods_includes(){
    include( twinwoods_DIR . '/admin.php' );
}
twinwoods_includes();
function thinwoods_enqueue_style() {
    wp_enqueue_style('booking.css', twinwoods_URL.'/booking.css' );
}
 
add_action( 'wp_enqueue_scripts', 'thinwoods_enqueue_style' );
add_shortcode("twinwoods", "twinwoods_shortcode_handler");

function twinwoods_shortcode_handler(){
    global $WOOCS; 
    ?>
    <script>
        //throw current currency rate to global scope
        var currency_rate=<?=(float) $WOOCS->get_currencies()[$WOOCS->current_currency]['rate'];?>;
    </script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="<?= get_template_directory_uri() ?>/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <div class="booking-wrap">
        
        <!--<input type="hidden" id="datepicker_value" value="08.08.2020">-->
        <div class="booking-products">
            <div class="twin-wrapper">
            <h2 class="twin-heading">Book your remote escape now!</h2>
            <input id="datepicker" type="text">
            <div class="currency-picker">
                <?php echo apply_filters('the_content', "[woocs]"); ?>
            </div>
            <?php
              $args = array(
                'post_type'      => 'product',
                'product_cat'    => get_option("twinwoods_category")
            );

            $loop = new WP_Query( $args );

            while ( $loop->have_posts() ) : $loop->the_post();
                global $product;
                ?>
                <div class="booking-item" data-id="<?= $product->get_id() ?>">
                    <ul class="bookind-slots bookind-slots<?= $product->get_id() ?>">
                        
                    </ul>
                    <span class="product-img"><?= $product->get_image('woocommerce_single') ?></span>
                    <div class="product-info">
                        <h3><?= $product->get_name(); ?></h3>
                        <div class="product-descr">
                            <?= $product->get_description(); ?>
                        </div>
                    </div>
                    <div class="product-controls">
                        <div>
                            <div class="monitor-input-wrap">
                                <button type="button" class="monitor-button minus" ></button>
                                <input type="number" autocomplete="off" name="monitor-quantity" min="2" max="10" value="2">
                                <button type="button" class="monitor-button plus" ></button>
                            </div>
                            <div class="monitor-caption">Number of screens:</div>
                        </div>
                        <span class="product-price"><span class="price-title">Price:</span><?= get_woocommerce_currency_symbol( get_woocommerce_currency() ) ?><span class="price"><?= floor(100*($product->get_price()+20*2*(float) $WOOCS->get_currencies()[$WOOCS->current_currency]['rate']))/100; ?></span>
                    </span>
                        <span class="startPrice" style="display:none;"><?= $product->get_price(); ?></span>
                        <button type="button" class="add-to-cart" disabled>Add to cart</button>
                        <div class="no-slots">Sorry, no slots left!</div>
                    </div>
                    
                </div>
                <?php
            endwhile;

            wp_reset_query();

            ?>
          </div>
        </div>
            <script type="text/javascript" src="<?= site_url() ?>/wp-content/plugins/twinwoods-booking/booking.js"></script>
        </div>
        <div class="twin-wrapper">
            <div class="twin-cart-wrap">
                
                <?php echo apply_filters('the_content', "[woocommerce_cart]"); ?>

            </div>
            <div class="checkout-wrap">
                <?php echo apply_filters('the_content', "[woocommerce_checkout]"); ?>
                <script type="text/javascript" src="<?= site_url() ?>/wp-content/plugins/woocommerce/assets/js/frontend/checkout.min.js"></script>
            </div>
        </div>
    </div>
    
    <?php
    //wp_enqueue_script("jquery-ui.js", "/wp-content/plugins/twinwoods-booking/jquery-ui.js");
}

add_action( 'wp_ajax_nopriv_twinwoods_add_to_cart', 'twinwoods_add_to_cart' );
add_action( 'wp_ajax_twinwoods_add_to_cart', 'twinwoods_add_to_cart' );
function twinwoods_add_to_cart(){
    //validate cart info
    global $WOOCS;
    $respond=array('result'=>false, 'message'=>'All fine');
    if(!is_int((int)$_POST['monitor_number'])){
        $respond['message']="Monitor number is invalid! ";
        echo json_encode($respond);
        wp_die();
    }
    if($_POST['monitor_number']<2){
        $respond['message']="Monitor number is too low!";
        echo json_encode($respond);
        wp_die();
    }
    if($_POST['monitor_number']>10){
        $respond['message']="Monitor number must me less than 10!";
        echo json_encode($respond);
        wp_die();
    }
    if(!is_int((int)$_POST['quantity']) or $_POST['quantity']<=0){
        $respond['message']="Quantity is invalid!";
        echo json_encode($respond);
        wp_die();
    }
    if(!is_int((int)$_POST['slot_id']) or $_POST['slot_id']<=0){
        $respond['message']="Slot is invalid!";
        echo json_encode($respond);
        wp_die();
    }
    global $wpdb;
    $slot=$wpdb->get_row("SELECT * FROM ".$wpdb->prefix."twinwoods_bookings WHERE id = ".$_POST['slot_id'], ARRAY_A);
    if (count($slot)<1){
        $respond['message']="Slot doesn't exists!";
        echo json_encode($respond);
        wp_die();
    }
    if ($slot['slots']<$_POST['quantity']){
        $respond['message']="Quantity is too high! Only ".$slot['slots']." free slots left";
        echo json_encode($respond);
        wp_die();
    }
    $respond['result']=true;
    //finish validation
    if (is_null($slot['booked_products'])){
        $booked=array();
    } else {
        $booked=unserialize($slot['booked_products']);
    }
    $product = wc_get_product( $_POST['product_id'] );
    array_push($booked, $_POST['product_id']);
    $cart_item_data = array('product_price'=>$product->get_price()/(float) $WOOCS->get_currencies()[$WOOCS->current_currency]['rate'], 'monitor_number'=>$_POST['monitor_number'], 'slot_id'=>(int)$_POST['slot_id'], 'timezone'=>(int)$_POST['timezone_offset'], 'timezone_name'=>$_POST['timezone_name'], 'begin'=> $slot['timestamp'], 'duration' => $slot['duration']);
    WC()->cart->add_to_cart( $_POST['product_id'], $_POST['quantity'], 0, array(), $cart_item_data );
    $wpdb->query("UPDATE ".$wpdb->prefix."twinwoods_bookings SET slots = slots - ".$_POST['quantity'].", booked_products = '".serialize($booked)."', slots_booked = slots_booked + ".$_POST['quantity']." WHERE id = ".$_POST['slot_id']);
    //$wpdb->update($wpdb->prefix."twinwoods_bookings", array('slots'=>$slot['slots']-$_POST['quantity'], 'booked_products'=>serialize($booked), 'slots_booked'=>$slot['slots_booked']+$_POST['quantity']), array('id'=>$_POST['slot_id']));
    echo json_encode($respond);
    wp_die();
}
add_action( 'woocommerce_after_cart_item_quantity_update', 'twinwoods_cart_update', 10, 4 );
function twinwoods_cart_update($cart_item_key, $quantity, $old_quantity, $that){
    $item=WC()->cart->get_cart_item($cart_item_key);
    if (!is_numeric($item['slot_id'])) {return true;}
    $diff=$old_quantity-$quantity;
    global $wpdb;
    if ($diff<0){
        $slot=$wpdb->get_row("SELECT * FROM ".$wpdb->prefix."twinwoods_bookings WHERE id = ".$item['slot_id'], ARRAY_A);
        if ($slot['slots']<$diff*-1){
            //not enough slots
            WC()->cart->set_quantity( $cart_item_key, $old_quantity);
            wc_add_notice('Not enough slots on this time!');
        } else {
            $wpdb->query("UPDATE ".$wpdb->prefix."twinwoods_bookings SET slots=slots - ".($diff*-1).", slots_booked = slots_booked +".($diff*-1)." WHERE id = ".$item['slot_id']);
        }

    }
    if ($diff>0){
        //error_log("UPDATE ".$wpdb->prefix."twinwoods_bookings SET slots=slots - ".$diff." WHERE id = ".$item['slot_id']);
        $wpdb->query("UPDATE ".$wpdb->prefix."twinwoods_bookings SET slots=slots + ".$diff.", slots_booked = slots_booked -".$diff." WHERE id = ".$item['slot_id']);
    }
}

add_filter( 'woocommerce_update_cart_validation', 'twinwoods_quant_validation', 10, 4 );
function twinwoods_quant_validation( $true, $cart_item_key, $values, $quantity){
    if(!$true){return false;}
    $item=WC()->cart->get_cart_item($cart_item_key);
    if (!is_numeric($item['slot_id'])) {return true;}
    global $wpdb;
    $slot=$wpdb->get_row("SELECT * FROM ".$wpdb->prefix."twinwoods_bookings WHERE id = ".$item['slot_id'], ARRAY_A);
    $quant=0;
    foreach ( WC()->cart->get_cart() as $cart_item_key => $values ) {
        if (is_numeric($values['slot_id']) and $values['slot_id']==$item['slot_id']){
            $quant+=$values['quantity'];
        }
    }
    if ($quant-$item['quantity']+$quantity-$slot['slots_booked']>$slot['slots'] )
    {
        wc_add_notice('Not enough slots on this time! Only '.$slot['slots'].' left');
        return false;
    }

}

add_action( 'woocommerce_remove_cart_item', 'twinwoods_item_removed', 10, 4 );
function twinwoods_item_removed($cart_item_key, $instance){
    $item=$instance->get_cart_item($cart_item_key);
    if (!is_numeric($item['slot_id'])) {return true;}
    $quant=$item['quantity'];
    global $wpdb;
    $wpdb->query("UPDATE ".$wpdb->prefix."twinwoods_bookings SET slots=slots + $quant, slots_booked = slots_booked - $quant WHERE id = ".$item['slot_id']);
}



add_action( 'wp_ajax_nopriv_twinwoods_reload_checkout', 'twinwoods_checkout_update', 10, 4 );
add_action( 'wp_ajax_twinwoods_reload_checkout', 'twinwoods_checkout_update', 10, 4 );
function twinwoods_checkout_update(){
    echo apply_filters('the_content', "[woocommerce_checkout]");
    wp_die();
}

add_action( 'woocommerce_before_calculate_totals', 'twinwoods_price_calc', 10, 1 );

function twinwoods_price_calc($cart_obj){
     //error_log(print_r($cart_obj->get_cart()));
     global $WOOCS; 
     foreach( $cart_obj->get_cart() as $key=>$value ) {
        if( isset( $value['monitor_number'] ) ) {
        $price = $value['product_price'];
        $price = $value['monitor_number']<=4 ? $value['monitor_number']*20+$price : 20*4+($value['monitor_number']-4)*15+ $price;
        $value['data']->set_price( $price );
}
     }
}

add_action( 'wp_ajax_get_slots', 'get_product_slots' );
function get_product_slots(){
    render_product_slots($_POST['timestamp']/1000, $_POST['product_id'], $_POST['offset']);
}

add_filter( 'woocommerce_cart_item_name', 'twin_display_info', 10, 3 );
function twin_display_info( $name, $cart_item, $cart_item_key ) {
    date_default_timezone_set('UTC');
    if( !isset( $cart_item['slot_id'] )) {return $name;}
    global $wpdb;
    $slot=$wpdb->get_row("SELECT * FROM ".$wpdb->prefix."twinwoods_bookings WHERE id = ".$cart_item['slot_id'], ARRAY_A);
    if (!$slot){
        return $name." Sorry, the slot was removed!";
    }
    $slot['timestamp']-=$cart_item['timezone']*60*1000;
    $name.="<br><div class='booking-info'><div class='booking-info-item'><div class='booking-info-heading'>Date</div><div class='booking-info-value'>";
    $name.=date('d/m/y', $slot['timestamp']/1000)."</div></div><div class='booking-info-item'><div class='booking-info-heading'>Time</div><div class='booking-info-value'>";
    $name.=date('h.i A', $slot['timestamp']/1000)." - ".date('h.i A', $slot['timestamp']/1000+$slot['duration']*3600)."</div></div><div class='booking-info-item'><div class='booking-info-heading'>Screens</div><div class='booking-info-value'>";
    $name.=$cart_item['monitor_number']."</div></div></div>";
    return $name;
}

add_action( 'woocommerce_checkout_create_order_line_item', 'twin_custom_data_to_order', 10, 4 );
function twin_custom_data_to_order( $item, $cart_item_key, $values, $order ) {
 foreach( $item as $cart_item_key=>$values ) {
 if( isset( $values['slot_id'] ) ) {
    global $wpdb;
    $slot=$wpdb->get_row("SELECT * FROM ".$wpdb->prefix."twinwoods_bookings WHERE id = ".$values['slot_id'], ARRAY_A);
    $slot['timestamp']-=$values['timezone']*60*1000;
    $item->add_meta_data( "Customer date", date('d/m/y', $slot['timestamp']/1000), true );
    $item->add_meta_data( "Customer time", date('h.i A', $slot['timestamp']/1000)." - ".date('h.i A', $slot['timestamp']/1000+$slot['duration']*3600), true );
    $item->add_meta_data( "Screens", $values['monitor_number'], true );
    $item->add_meta_data( "Timestamp",  $slot['timestamp']/1000, true );
    $item->add_meta_data( "Duration",  $slot['duration']*3600, true );
    $item->add_meta_data( "Customer timezone offset",  $values['timezone'], true );
    $item->add_meta_data( "Customer timezone name",  $values['timezone_name'], true );
 }
 }
}



function render_product_slots($timestamp, $id, $timezoneOffset=0){
    //for product admin
    date_default_timezone_set('UTC');
    global $wpdb;
    $date=date("Y-m-d", $timestamp+$timezoneOffset);
    $slots=$wpdb->get_results( "SELECT * FROM ".$wpdb->prefix."twinwoods_bookings WHERE DATE(time) = '$date' AND product_id = $id", ARRAY_A);
    foreach ($slots as $slot) {
        ?>
        <li id="id<?= $slot['id'] ?>" data-id="<?= $slot['id'] ?>" 
            data-utc-time="<?= $slot['time'] ?>" 
            data-stamp="<?= $slot['timestamp'] ?>" 
            data-duration="<?= $slot['duration'] ?>">
            <button class="delete-slot" type="button">X</button>
            <div class="time-period">
                <span class="time-begin">
                </span> - 
                <span class="time-end">
                </span>
            </div>
            <div class="slots-info" style="display:none;">
                <span><?= $slot['slots'] ?> left</span>&nbsp;&nbsp; &nbsp;&nbsp;<span><?= $slot['slots_booked'] ?> booked</span>
            </div>
            
        </li>
        <script type="text/javascript">
            console.log("Dates executed");
            document.getElementById("id<?= $slot['id'] ?>").querySelector(".time-begin").innerHTML=new Date(<?= $slot['timestamp'] ?>+timezoneOffset).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
            document.getElementById("id<?= $slot['id'] ?>").querySelector(".time-end").innerHTML=new Date(<?= $slot['timestamp'] ?>+timezoneOffset+<?= $slot['duration'] ?>*3600000).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
        </script>
        <?php
    }
die();
}

add_action( 'wp_ajax_nopriv_twin_update_cart', 'twin_update_cart' );
add_action( 'wp_ajax_twin_update_cart', 'twin_update_cart' );
function twin_update_cart(){
    echo apply_filters('the_content', "[woocommerce_cart]"); 
    wp_die();
}
add_action( 'wp_ajax_nopriv_get_product_slots', 'view_product_slots' );
add_action( 'wp_ajax_get_product_slots', 'view_product_slots' );
function view_product_slots(){
    view_bookable_slots($_POST['timestamp']/1000, $_POST['product_id'], $_POST['offset']);
}
function view_bookable_slots($timestamp, $id, $timezoneOffset=0){
    //display for product
    date_default_timezone_set('UTC');
    global $wpdb;
    $date=date("Y-m-d", $timestamp+$timezoneOffset);
    $slots=$wpdb->get_results( "SELECT * FROM ".$wpdb->prefix."twinwoods_bookings WHERE DATE(time) = '$date' AND product_id = $id AND slots > 0", ARRAY_A);
    if (count($slots) == 0){
        die();
    }
    foreach ($slots as $slot) {
        ?>
        <li id="id<?= $slot['id'] ?>" data-id="<?= $slot['id'] ?>">
        <label>
            <input type="radio" name="prodid-<?= $id ?>" value="<?= $slot['id'] ?>">
            <div class="booking-slot-wrap" 
            data-utc-time="<?= $slot['time'] ?>" 
            data-stamp="<?= $slot['timestamp'] ?>" 
            data-duration="<?= $slot['duration'] ?>">
            <div class="time-period">
                <span class="time-begin">
                </span> - 
                <span class="time-end">
                </span>
            </div>
            <div class="slots-info" style="display:none;">
                <span><?= $slot['slots'] ?> left</span>
            </div>
            </div>
        </label>
        </li>
        <script type="text/javascript">
            console.log("Dates executed");
            document.getElementById("id<?= $slot['id'] ?>").querySelector(".time-begin").innerHTML=new Date(<?= $slot['timestamp'] ?>+timezoneOffset).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
            document.getElementById("id<?= $slot['id'] ?>").querySelector(".time-end").innerHTML=new Date(<?= $slot['timestamp'] ?>+timezoneOffset+<?= $slot['duration'] ?>*3600000).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
        </script>
        <?php
    }
die();
}

function booking_slots(){
    global $product;
    ?>
    <style>
        .loading{
            color:#FFFFFF00;
            background-image:url(<?=twinwoods_URL?>/ajax-loader.gif);
            background-repeat: no-repeat;
            background-position: center;
            background-size:contain;
        }
        .add-slot-wrap label {
            float: none;
            margin: 0;
        }
        .add-slot-wrap input{
            float:none !important;
            width:250px !important;
        }
        .slots-list {
            display: flex;
            flex-wrap: wrap;
        }
        .slots-list li{
            border: 2px solid gray;
            padding: 10px;
            margin-right: 10px;
            margin-bottom: 10px;
            position: relative;
            border-radius: 10px;
        }
        button.delete-slot {
            position: absolute;
            top: 2px;
            right: 2px;
            width: 12px;
            height: 12px;
            padding: 0;
            font-size: 11px;
            line-height: 7px;
        }
    </style>
    <script type="text/javascript">
        const timezoneOffset=new Date().getTimezoneOffset()*60*1000*-1*0;
    </script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    <div class="wrap">
      <h1><?php esc_html( get_admin_page_title() ); ?></h1>
      <div>
        <h1> Booking slots </h1>
        <div id="datepicker"></div>
        <ul class="slots-list">
            Pick a date...
        </ul>
        <div class="add-slot-wrap">
            <h1>Add new slot</h1>
            <input type="hidden" name="product-id" value="<?= get_the_ID(); ?>">
            <label>
                Beginning time<br>
                <input type="text" class="timepicker" name="time" value="6:00 AM">
            </label>
            <label><br>
                Duration (hrs)<br>
                <input type="number" name="duration" step="0.5" min="0" value="1.5">
            </label><br>
            <label style="display:none">
                Available slots<br>
                <input type="number" name="slots" step="1" min="1" value="1">
            </label><br>
            <label>
                Repeat:<br>
                <input type="number" name="repeat" step="1" min="1" value="1">
            </label><br>
            <button type="button" id="add_slots">Add new slots</button>
        </div>
      </div>
      <script type="text/javascript">
            $("#datepicker").datepicker({
                onSelect: function(date){
                    getSlots(date);
                }
            });
            $("#datepicker").datepicker("setDate", Date.now());
            function getSlots(date){
                document.querySelector('.slots-list').innerHTML='';
                    let timestamp=new Date(date).getTime();
                    let params={
                    url: "/wp-admin/admin-ajax.php",
                    method:"POST",
                    data:{
                        action:"get_slots",
                        timestamp:timestamp,
                        product_id:<?= get_the_ID(); ?>,
                        offset:new Date().getTimezoneOffset()*60*-1
                    }
                    };
                    $.ajax(params)
                    .done(function(res){
                        document.querySelector('.slots-list').innerHTML=res;
                        [].forEach.call(document.querySelectorAll('.slots-list script'), function(item){
                            eval(item.innerHTML);
                        });
                    })
                    .fail(function(err){
                        alert("Something is wrong with loading time slots! "+JSON.stringify(err));
                    })
                    .always(function(){

                    });
            };
            getSlots($("#datepicker").val());
           $('.timepicker').timepicker({
                timeFormat: 'h:mm p',
                interval: 30,
                startTime: '6:00',
                default:'8:00',
                dynamic: false,
                dropdown: true,
                scrollbar: true
            });
            $("#add_slots").on("click", function(){
                this.classList.add("loading");
                let product_id=$('[name="product-id"]').val(),
                    date=new Date($("#datepicker").val()+" "+$(".timepicker").val()),
                    timestamp=date.getTime(),
                    duration=$('[name="duration"]').val(),
                    slots=$('[name="slots"]').val(),
                    repeat=$('[name="repeat"]').val();
                window.z=date;
                date=date.getUTCFullYear()+":"+(date.getUTCMonth()+1)+":"+(date.getUTCDate()+1)+" "+date.getUTCHours()+":"+date.getUTCMinutes()+":00";
                let params={
                    url: "/wp-admin/admin-ajax.php",
                    method:"POST",
                    data:{
                        action:"add_slots",
                        date:date,
                        product_id:product_id,
                        timestamp:timestamp,
                        duration:duration,
                        slots:slots,
                        repeat:repeat
                    }
                };
                $.ajax(params)
                .done(function(res){
                    let result=JSON.parse(res);
                    if (!result['result']) alert(result['message']);
                    getSlots($("#datepicker").val());
                })
                .fail(function(){
                    alert("Can't add slots, something is wrong!");
                })
                .always(function(){
                    $("#add_slots").removeClass("loading");
                });
            });
            $("body").on("click", function(e){
                if (!e.target.classList.contains("delete-slot")) return;
                e.target.classList.add("loading");
                let product_id=$('[name="product-id"]').val(),
                    date=new Date($("#datepicker").val()+" "+$(".timepicker").val()),
                    timestamp=date.getTime(),
                    duration=$('[name="duration"]').val(),
                    slots=$('[name="slots"]').val(),
                    repeat=$('[name="repeat"]').val();
                window.z=date;
                date=date.getUTCFullYear()+":"+(date.getUTCMonth()+1)+":"+(date.getUTCDate()+1)+" "+date.getUTCHours()+":"+date.getUTCMinutes()+":00";
                let params={
                    url: "/wp-admin/admin-ajax.php",
                    method:"POST",
                    data:{
                        action:"remove_slot",
                        slot_id: $(e.target).closest('li').data('id')
                    }
                };
                $.ajax(params)
                .done(function(){
                    getSlots($("#datepicker").val());
                })
                .fail(function(){
                    alert("Can't add slots, something is wrong!");
                })
                .always(function(){
                    $(this).removeClass("loading");
                }.bind(e.target));
            });
      </script>
    </div>
     
<?php
}
add_action( 'woocommerce_product_options_general_product_data', 'booking_slots' );

register_activation_hook( __FILE__, 'twinwoods_activate' );
function twinwoods_activate(){
  global $wpdb;
  $wpdb->query("CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."twinwoods_bookings`(
    `id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `product_id` INT NOT NULL,
    `time` DATETIME NOT NULL,
    `timestamp` MEDIUMTEXT NOT NULL,
    `duration` FLOAT NOT NULL DEFAULT 1,
    `slots` INTEGER NOT NULL DEFAULT 1,
    `slots_booked` INTEGER NOT NULL DEFAULT 0,
    `booked_products` MEDIUMTEXT
    );");
}

