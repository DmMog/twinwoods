<?php /* Template Name: Checkout */ ?>

<?php
get_header();
?>
<link rel="stylesheet" href="<?= site_url() ?>/wp-content/plugins/twinwoods-booking/booking.css">
	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php

			// Start the Loop.
			while ( have_posts() ) :
				the_post();?>
				<div class="checkout-wrap">
				<?php
				the_content();
				?>
				</div>
				<?php
				// If comments are open or we have at least one comment, load up the comment template.

			endwhile; // End the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
