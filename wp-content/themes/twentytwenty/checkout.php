<?php /* Template Name: Checkout */ ?>

<?php
get_header();
?>
<style>
	[name="checkout"]>*{
	float: none !important;
    width: 100% !important;
}
</style>
<link rel="stylesheet" href="<?= site_url() ?>/wp-content/plugins/twinwoods-booking/booking.css">
<div class="twin-wrapper">
			<?php

			// Start the Loop.
			while ( have_posts() ) :
				the_post();?>
				<div class="checkout-wrap">
				<?php
				the_content();
				?>
				</div>
				<?php
				// If comments are open or we have at least one comment, load up the comment template.

			endwhile; // End the loop.
			?>



<?php
get_footer();
