<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'twinwoods' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );
//for dev u{@lOmhnJfTj
/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'YS&+gGt;Ei9Q:U+Gaq{E!T!_!N+>TNwSfEN`epA}z5bW)J?V$<y_CN<0xafT2E,Y' );
define( 'SECURE_AUTH_KEY',  'eCTo;$vNpZ*:A@gx5$-I>:]K?I-csgr0D:hf|&I}D)ilhsRicPgk~3_WK33L&>.n' );
define( 'LOGGED_IN_KEY',    '/033w/g[h;5H>C%na~ JTrpq,OH?2Q+_!p-]}{mz%/_p6#U$c;}J# tp(#qp>l6&' );
define( 'NONCE_KEY',        'JO{QW(SF^dQ)t:$u5hDG_;dI :zZKz*%LyRsVcvDT_Fw^y`El(o]~0#Ydp>z%vU6' );
define( 'AUTH_SALT',        'sp_giVfkx]jlKzeN#V)H(pnnjYij==3n_J0vKlJx~vasZ])r?b7M H{9(gh 7HK{' );
define( 'SECURE_AUTH_SALT', 'a3|BHHG+zI8w|D$r7K_O:Ho3G0&!X.HqhP/p8;`owzq2/9,QFIPLzsMl7S< G=K?' );
define( 'LOGGED_IN_SALT',   'YfEXgFrVZ9l>|D4O*J@2Ld4`uUZas{{e=]gm7E._%Ev<]I=1<!nJ~  YRRuQ%4nZ' );
define( 'NONCE_SALT',       'cFX%hZs{8+Z%3pVT96!6?x*Ye*s{2o8K,!v[~svur ml,,CKdmDzQ]V=0gVsZ0}t' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );
define( 'WP_DEBUG_LOG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
